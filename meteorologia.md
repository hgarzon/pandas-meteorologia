# Análisis de datos de un dataset usando la librería Pandas de Python
En este artículo vamos a ver un ejemplo de análisis de datos mediante la librería Pandas de Python. 
Se va a ver los distintos procesos que se require realizar para llevar a cabo esta tarea, ya que no solo consiste en analizar los datos sino que abarca el proceso de carga de los datos, limpiar los datos y por último obtener información relevante de los datos mediante el análisis de los mismos. 
Los 2 primeros procesos: obtención de los datos y limpieza de los mismos son procesos que son necesarios también en el Machine Learning (de hecho suponen cerca del 80% del tiempo empleado), por lo que son procesos importantes y que deben realizarse con cuidado ya que la calidad del resultado del análisis o del modelo generado en el caso de Machine Learning dependen en gran medida de la calidad de los datos ("Trash in trash out")

El artículo se ha realizado en un notebook de Jupyter para que tanto el código como la documentación esten en un mismo sitio. El código puede ser ejecutado desde una terminal de forma normal (se puede extrar el código del notebook de forma sencilla desde la interfaz de Jupyter)
Para instalar Jupyter puede serguir las instrucciones indicadas en la web oficial: https://jupyter.org/install o usar docker: https://github.com/jupyter/docker-stacks

# 1. Caso de estudio y objetivo del análisis
Se va a utilizar los datos que provee el AMAET (Agencia estatal de Meteorologia). Se ha usado los enlaces disponibles en la web https://datosclima.es/Aemet2013/DescargaDatos.html ya que es mas sencillo su descarga a traves de esa web

Los datos contienen información sobre temperatura (máxima, mínima y media), viento (racha y velocidad máxima de las medias) y precipitaciones (total y por tramo horario) para cada estación meteorologica de España.
Los datos son diarios.
En algunos casos falta información y eso se tiene que tener en cuenta durante el proceso de limpieza de datos.
Se ha analizado únicamente el mes de Enero por simplificar el dataset.

El análisis que se va a realizar en este artículo pretende ser simplemente un ejemplo de lo sencillo que es realizar operaciones sobre los datos mediante la librería Pandas y va a consistir en obtener ciertos valores para tener una vision del clima en las distintas provincias de España.

# 2. Pandas y Python
Para el proceso se va a usar el lenguaje Python ya que es uno de los lenguajes mas usados para el análisis de datos, asi como para Machine Learning

La version de Python usada es la 3

Pandas (https://pandas.pydata.org) es la librería de facto para el tratamiento y análisis de datos cuando usamos Python
Nos proporciona la clase Dataframe la cual permite almacenar los datos en un objeto similar a una tabla de una base de datos.
Lo que hace a esta librería ser realmente útil para el análisis de datos son los métodos que nos proporciona, los cuales nos permiten realizar tareas de ordenación, agrupación, etc de forma sencilla y rápida.

# 3. Implementación

## 3.1 Importación de las librerias necesarias
Importamos las librerías que vamos a usar:
- **Pandas:** libería principal para la carga y el análisis de datos
- **Glob:** librería para la gestión de ficheros. En nuestro caso se usa únicamente para realizar la carga de varios ficheros de forma más sencilla
- **Numpy:** nos proporciona métodos de gestión de arrays


```python
#!pip install pandas
#!pip install numpy
import pandas as pd
import glob
import numpy as np
```

### Carga de datos
Pandas nos permite obtener los datos de diversas fuentes, principalmente ficheros. Además, podemos leer directamente de distintos formatos de fichero: xls, csv, json, etc. 
En nuestro caso leeremos ficheros xls
Es importante también tener en cuenta que se puede escribir en fichero el contenido de los datos, es decir, podemos leer los datos, modificarlos y escribirlos de nuevo en fichero. De esta forma podemos hacer procesos de modificación de datos de forma muy sencilla

En nuestro caso, obtenemos los ficheros rar que contienen las hojas de cálculo con los datos. Cada fichero rar contiene los ficheros xls correspondientes a cada día de ese mes. Para nuestro análisis hemos cogido solo 1 mes (Enero), pero sería igual de sencillo cargar más datos.


```python
!wget https://datosclima.es/capturadatos/Aemet2019-01.rar
```

    --2020-07-09 18:10:33--  https://datosclima.es/capturadatos/Aemet2019-01.rar
    Resolving datosclima.es (datosclima.es)... 95.217.108.87
    Connecting to datosclima.es (datosclima.es)|95.217.108.87|:443... connected.
    HTTP request sent, awaiting response... 200 OK
    Length: 1456378 (1.4M) [application/x-rar-compressed]
    Saving to: ‘Aemet2019-01.rar.9’
    
    Aemet2019-01.rar.9  100%[===================>]   1.39M  3.00MB/s    in 0.5s    
    
    2020-07-09 18:10:33 (3.00 MB/s) - ‘Aemet2019-01.rar.9’ saved [1456378/1456378]
    



```python
# Instala unrar si es necesario
!unrar e -o+ Aemet2019-01.rar
```

    
    UNRAR 5.50 freeware      Copyright (c) 1993-2017 Alexander Roshal
    
    
    Extracting from Aemet2019-01.rar
    
    Extracting  Aemet2019-01-22.xls                                            3  OK 
    Extracting  Aemet2019-01-23.xls                                            6  OK 
    Extracting  Aemet2019-01-24.xls                                           10  OK 
    Extracting  Aemet2019-01-25.xls                                         1 13  OK 
    Extracting  Aemet2019-01-26.xls                                         1 16  OK 
    Extracting  Aemet2019-01-27.xls                                         1 19  OK 
    Extracting  Aemet2019-01-28.xls                                         2 23  OK 
    Extracting  Aemet2019-01-29.xls                                         2 26  OK 
    Extracting  Aemet2019-01-30.xls                                         2 30  OK 
    Extracting  Aemet2019-01-31.xls                                         3 33  OK 
    Extracting  Aemet2019-01-01.xls                                         36  OK 
    Extracting  Aemet2019-01-02.xls                                         39  OK 
    Extracting  Aemet2019-01-03.xls                                         42  OK 
    Extracting  Aemet2019-01-04.xls                                         45  OK 
    Extracting  Aemet2019-01-05.xls                                         4 49  OK 
    Extracting  Aemet2019-01-06.xls                                         5 52  OK 
    Extracting  Aemet2019-01-07.xls                                         5 55  OK 
    Extracting  Aemet2019-01-08.xls                                         5 58  OK 
    Extracting  Aemet2019-01-09.xls                                         6 61  OK 
    Extracting  Aemet2019-01-10.xls                                         6 64  OK 
    Extracting  Aemet2019-01-11.xls                                         6 67  OK 
    Extracting  Aemet2019-01-12.xls                                         6 70  OK 
    Extracting  Aemet2019-01-13.xls                                         7 73  OK 
    Extracting  Aemet2019-01-14.xls                                         7 76  OK 
    Extracting  Aemet2019-01-15.xls                                         7 79  OK 
    Extracting  Aemet2019-01-16.xls                                         8 82  OK 
    Extracting  Aemet2019-01-17.xls                                         8 86  OK 
    Extracting  Aemet2019-01-18.xls                                         8 89  OK 
    Extracting  Aemet2019-01-19.xls                                         9 92  OK 
    Extracting  Aemet2019-01-20.xls                                         9 96  OK 
    Extracting  Aemet2019-01-21.xls                                         9 99  OK 
    All OK


Recorremos los ficheros xls ya descomprimidos y los vamos cargando en un objeto dataframe.
Si vemos el contenido de los ficheros xls podemos observar que no contienen la fecha de los datos, lo cual es un dato importante porque queremos ordenar los datos en algunos casos. Para obtener este dato vamos a usar el nombre del fichero, el cual contiene la fecha a la que corresponden los datos. Vamos a incorporar una columa adicional al dataset con el nombre del fichero que estamos leyendo.


```python
all_files = glob.glob("Aemet2019-*.xls")

file_list = []
for f in all_files:
    data = pd.read_excel(f,skiprows=4)
    data['source_file'] = f # nueva columna conteniendo el nombre del fichero leido
    file_list.append(data)
df = pd.concat(file_list)
```

Vamos a ver cuantas filas y columnas contiene nuestro dataset


```python
df.shape
```




    (24707, 13)



Vemos que tiene 24707 filas y 13 columnas

Veamos las 5 primeras filas para ver como son los datos:


```python
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Estación</th>
      <th>Provincia</th>
      <th>Temperatura máxima (ºC)</th>
      <th>Temperatura mínima (ºC)</th>
      <th>Temperatura media (ºC)</th>
      <th>Racha (km/h)</th>
      <th>Velocidad máxima (km/h)</th>
      <th>Precipitación 00-24h (mm)</th>
      <th>Precipitación 00-06h (mm)</th>
      <th>Precipitación 06-12h (mm)</th>
      <th>Precipitación 12-18h (mm)</th>
      <th>Precipitación 18-24h (mm)</th>
      <th>source_file</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Estaca de Bares</td>
      <td>A Coruña</td>
      <td>11.2 (15:30)</td>
      <td>10.0 (03:40)</td>
      <td>10.6</td>
      <td>64 (22:20)</td>
      <td>49 (22:20)</td>
      <td>1.6</td>
      <td>0.0</td>
      <td>1.6</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>Aemet2019-01-09.xls</td>
    </tr>
    <tr>
      <th>1</th>
      <td>As Pontes</td>
      <td>A Coruña</td>
      <td>10.7 (14:00)</td>
      <td>4.6 (05:30)</td>
      <td>7.6</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>Aemet2019-01-09.xls</td>
    </tr>
    <tr>
      <th>2</th>
      <td>A Coruña</td>
      <td>A Coruña</td>
      <td>13.1 (12:30)</td>
      <td>7.4 (02:40)</td>
      <td>10.3</td>
      <td>41 (18:20)</td>
      <td>28 (15:40)</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>Aemet2019-01-09.xls</td>
    </tr>
    <tr>
      <th>3</th>
      <td>A Coruña Aeropuerto</td>
      <td>A Coruña</td>
      <td>12.6 (15:50)</td>
      <td>-0.8 (05:40)</td>
      <td>5.9</td>
      <td>35 (15:20)</td>
      <td>23 (15:20)</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>Aemet2019-01-09.xls</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Carballo, Depuradora</td>
      <td>A Coruña</td>
      <td>11.6 (13:40)</td>
      <td>-1.4 (03:20)</td>
      <td>5.1</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>Aemet2019-01-09.xls</td>
    </tr>
  </tbody>
</table>
</div>



## 3.2 Limpieza de datos
Una parte importante del proceso de obtención de datos es limpiar su contenido para que sea adecuado a nuestras necesidades y para hacerlos mas cómodos de usar

Podemos empezar renombrando las columnas para que sea mas sencillo usarlas y ocupen menos espacio en pantalla


```python
df=df.rename(columns={'Estación': 'estacion','Provincia':'provincia','Temperatura máxima (ºC)':'temp_max','Temperatura mínima (ºC)':'temp_min','Temperatura media (ºC)':'temp_med','Racha (km/h)':'viento_racha','Velocidad máxima (km/h)':'viento_vel_max','Precipitación 00-24h (mm)':'prec_dia','Precipitación 00-06h (mm)':'prec_0_6h','Precipitación 06-12h (mm)':'prec_6_12h','Precipitación 12-18h (mm)':'prec_12_18h','Precipitación 18-24h (mm)':'prec_18_24h','source_file':'fecha'})
df.head(1)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>estacion</th>
      <th>provincia</th>
      <th>temp_max</th>
      <th>temp_min</th>
      <th>temp_med</th>
      <th>viento_racha</th>
      <th>viento_vel_max</th>
      <th>prec_dia</th>
      <th>prec_0_6h</th>
      <th>prec_6_12h</th>
      <th>prec_12_18h</th>
      <th>prec_18_24h</th>
      <th>fecha</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Estaca de Bares</td>
      <td>A Coruña</td>
      <td>11.2 (15:30)</td>
      <td>10.0 (03:40)</td>
      <td>10.6</td>
      <td>64 (22:20)</td>
      <td>49 (22:20)</td>
      <td>1.6</td>
      <td>0.0</td>
      <td>1.6</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>Aemet2019-01-09.xls</td>
    </tr>
  </tbody>
</table>
</div>



Podemos observar que en algunas columnas ademas del valor numérico también se tiene la hora, lo cual no nos interesa ya que los datos tienen que ser numéricos para nuestro análisis. Asi que vamos a quitar esa parte de los datos


```python
df=df.replace(to_replace=r'.\(.+\)$', value='', regex=True)
df.head(1)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>estacion</th>
      <th>provincia</th>
      <th>temp_max</th>
      <th>temp_min</th>
      <th>temp_med</th>
      <th>viento_racha</th>
      <th>viento_vel_max</th>
      <th>prec_dia</th>
      <th>prec_0_6h</th>
      <th>prec_6_12h</th>
      <th>prec_12_18h</th>
      <th>prec_18_24h</th>
      <th>fecha</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Estaca de Bares</td>
      <td>A Coruña</td>
      <td>11.2</td>
      <td>10.0</td>
      <td>10.6</td>
      <td>64</td>
      <td>49</td>
      <td>1.6</td>
      <td>0.0</td>
      <td>1.6</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>Aemet2019-01-09.xls</td>
    </tr>
  </tbody>
</table>
</div>



La columna que hemos creado para disponer del nombre del fichero y asi disponer de la fecha de los datos contiene más texto además de la fecha. Vamos a limpiar el valor para dejar solo la fecha


```python
df.fecha=df.fecha.replace(regex={'Aemet':'',r'\.+xls':''})
df.head(1)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>estacion</th>
      <th>provincia</th>
      <th>temp_max</th>
      <th>temp_min</th>
      <th>temp_med</th>
      <th>viento_racha</th>
      <th>viento_vel_max</th>
      <th>prec_dia</th>
      <th>prec_0_6h</th>
      <th>prec_6_12h</th>
      <th>prec_12_18h</th>
      <th>prec_18_24h</th>
      <th>fecha</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Estaca de Bares</td>
      <td>A Coruña</td>
      <td>11.2</td>
      <td>10.0</td>
      <td>10.6</td>
      <td>64</td>
      <td>49</td>
      <td>1.6</td>
      <td>0.0</td>
      <td>1.6</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>2019-01-09</td>
    </tr>
  </tbody>
</table>
</div>



Vamos a ver de que tipo son las columnas actualmente. El tipo se puede definir explícitamente al leer el fichero, pero para este artículo hemos dejado que Pandas lo calcule automáticamente


```python
df.info()
```

    <class 'pandas.core.frame.DataFrame'>
    Int64Index: 24707 entries, 0 to 796
    Data columns (total 13 columns):
     #   Column          Non-Null Count  Dtype  
    ---  ------          --------------  -----  
     0   estacion        24707 non-null  object 
     1   provincia       24707 non-null  object 
     2   temp_max        23650 non-null  object 
     3   temp_min        23650 non-null  object 
     4   temp_med        23650 non-null  float64
     5   viento_racha    20072 non-null  object 
     6   viento_vel_max  20203 non-null  object 
     7   prec_dia        23366 non-null  float64
     8   prec_0_6h       23565 non-null  float64
     9   prec_6_12h      23565 non-null  float64
     10  prec_12_18h     23584 non-null  float64
     11  prec_18_24h     23563 non-null  float64
     12  fecha           24707 non-null  object 
    dtypes: float64(6), object(7)
    memory usage: 2.6+ MB


Vemos que hay columnas con tipo "object" cuando debería ser un float (por ejemplo: temp_max y viento_racha). Esto es porque hay filas que no tienen valor para esas columnas, luego veremos que hacer con ellas.
Por ahora vamos a poner los tipos adecuados a cada columna


```python
df=df.astype({'estacion': 'string','provincia': 'string', 'temp_max':'float64', 'temp_min':'float64', 'viento_racha':'float64', 'viento_vel_max':'float64','fecha': 'datetime64[ns]'})
df['fecha']=pd.to_datetime(df["fecha"].dt.strftime('%Y-%m-%d'))
df.info()
```

    <class 'pandas.core.frame.DataFrame'>
    Int64Index: 24707 entries, 0 to 796
    Data columns (total 13 columns):
     #   Column          Non-Null Count  Dtype         
    ---  ------          --------------  -----         
     0   estacion        24707 non-null  string        
     1   provincia       24707 non-null  string        
     2   temp_max        23650 non-null  float64       
     3   temp_min        23650 non-null  float64       
     4   temp_med        23650 non-null  float64       
     5   viento_racha    20072 non-null  float64       
     6   viento_vel_max  20203 non-null  float64       
     7   prec_dia        23366 non-null  float64       
     8   prec_0_6h       23565 non-null  float64       
     9   prec_6_12h      23565 non-null  float64       
     10  prec_12_18h     23584 non-null  float64       
     11  prec_18_24h     23563 non-null  float64       
     12  fecha           24707 non-null  datetime64[ns]
    dtypes: datetime64[ns](1), float64(10), string(2)
    memory usage: 2.6 MB


Vamos a eliminar las estaciones que tienen filas a las que les faltan datos para estandarizar los datos. 
Realmente habria que intentar rellenar los datos que faltan mediante alguna de las técnicas recomendadas (media, propagate last valid observation forward, etc) pero para nuestro ejemplo vamos a simplemente eliminar filas por simplificar


```python
estaciones_con_nan=df[df.isnull().any(axis=1)]['estacion'].unique()
df.drop(df[df['estacion'].isin(estaciones_con_nan)].index, inplace=True)
df.isna().sum()
```




    estacion          0
    provincia         0
    temp_max          0
    temp_min          0
    temp_med          0
    viento_racha      0
    viento_vel_max    0
    prec_dia          0
    prec_0_6h         0
    prec_6_12h        0
    prec_12_18h       0
    prec_18_24h       0
    fecha             0
    dtype: int64



## 3.3 Análisis de datos

Ahora que ya tenemos los datos limpios vamos a empezar a analizarlos, demostrando que fácil es realizarlo con pandas

### Primeros contactos con los datos 

Vamos a usar el método "describe" para ver de forma sencilla algunas medidas de nuestros datos.
También es un ejemplo de como podemos seleccionar ciertas columnas por su indice mediante el uso de la librería numpy


```python
df.iloc[:,np.r_[0:8,12:13]].describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>temp_max</th>
      <th>temp_min</th>
      <th>temp_med</th>
      <th>viento_racha</th>
      <th>viento_vel_max</th>
      <th>prec_dia</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>14663.000000</td>
      <td>14663.000000</td>
      <td>14663.000000</td>
      <td>14663.000000</td>
      <td>14663.000000</td>
      <td>14663.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>12.558869</td>
      <td>2.557737</td>
      <td>7.560342</td>
      <td>34.010434</td>
      <td>19.691741</td>
      <td>1.490009</td>
    </tr>
    <tr>
      <th>std</th>
      <td>4.506435</td>
      <td>5.256177</td>
      <td>4.350412</td>
      <td>17.793154</td>
      <td>11.139909</td>
      <td>5.890856</td>
    </tr>
    <tr>
      <th>min</th>
      <td>-5.000000</td>
      <td>-20.500000</td>
      <td>-7.400000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>9.500000</td>
      <td>-1.300000</td>
      <td>4.500000</td>
      <td>20.000000</td>
      <td>12.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>12.700000</td>
      <td>2.100000</td>
      <td>7.300000</td>
      <td>30.000000</td>
      <td>17.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>15.700000</td>
      <td>5.900000</td>
      <td>10.200000</td>
      <td>45.000000</td>
      <td>26.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>27.800000</td>
      <td>19.100000</td>
      <td>21.100000</td>
      <td>146.000000</td>
      <td>113.000000</td>
      <td>132.800000</td>
    </tr>
  </tbody>
</table>
</div>



Ahora vamos a mostrar 5 filas al azar como muestreo de los datos. 
También es un ejemplo de selección de columnas por expresion regular (seleccionamos las que empiezan por el prefijo "temp_"


```python
df.filter(regex='^temp_*', axis=1).sample(5)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>temp_max</th>
      <th>temp_min</th>
      <th>temp_med</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>36</th>
      <td>19.8</td>
      <td>3.8</td>
      <td>11.8</td>
    </tr>
    <tr>
      <th>285</th>
      <td>8.3</td>
      <td>5.7</td>
      <td>7.0</td>
    </tr>
    <tr>
      <th>642</th>
      <td>8.5</td>
      <td>-4.7</td>
      <td>1.9</td>
    </tr>
    <tr>
      <th>48</th>
      <td>17.4</td>
      <td>12.4</td>
      <td>14.9</td>
    </tr>
    <tr>
      <th>729</th>
      <td>11.8</td>
      <td>2.5</td>
      <td>7.2</td>
    </tr>
  </tbody>
</table>
</div>



### Medias por provincia

Vamos a mostrar a continuación la media de los valores de cada columna agrupando por provincia.
Podemos observar que el método es lo suficientemente inteligente para mostrarnos solo columnas de tipo numérico


```python
df.groupby('provincia').mean()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>temp_max</th>
      <th>temp_min</th>
      <th>temp_med</th>
      <th>viento_racha</th>
      <th>viento_vel_max</th>
      <th>prec_dia</th>
      <th>prec_0_6h</th>
      <th>prec_6_12h</th>
      <th>prec_12_18h</th>
      <th>prec_18_24h</th>
    </tr>
    <tr>
      <th>provincia</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>A Coruña</th>
      <td>12.402765</td>
      <td>5.505991</td>
      <td>8.958065</td>
      <td>43.552995</td>
      <td>27.073733</td>
      <td>3.329954</td>
      <td>0.540553</td>
      <td>1.136866</td>
      <td>0.994009</td>
      <td>0.658525</td>
    </tr>
    <tr>
      <th>Alacant/Alicante</th>
      <td>15.780645</td>
      <td>3.768664</td>
      <td>9.777880</td>
      <td>32.852535</td>
      <td>18.253456</td>
      <td>0.070968</td>
      <td>0.031336</td>
      <td>0.003687</td>
      <td>0.017512</td>
      <td>0.018433</td>
    </tr>
    <tr>
      <th>Albacete</th>
      <td>12.548387</td>
      <td>-0.327419</td>
      <td>6.117204</td>
      <td>32.806452</td>
      <td>17.376344</td>
      <td>0.114516</td>
      <td>0.031183</td>
      <td>0.024194</td>
      <td>0.007527</td>
      <td>0.051613</td>
    </tr>
    <tr>
      <th>Almería</th>
      <td>16.347312</td>
      <td>7.915591</td>
      <td>12.132258</td>
      <td>40.731183</td>
      <td>27.166667</td>
      <td>0.101613</td>
      <td>0.061828</td>
      <td>0.007527</td>
      <td>0.005376</td>
      <td>0.026882</td>
    </tr>
    <tr>
      <th>Araba/Álava</th>
      <td>7.577419</td>
      <td>1.049194</td>
      <td>4.316129</td>
      <td>32.395161</td>
      <td>17.387097</td>
      <td>5.145161</td>
      <td>1.417742</td>
      <td>1.216129</td>
      <td>0.879032</td>
      <td>1.632258</td>
    </tr>
    <tr>
      <th>Asturias</th>
      <td>11.186380</td>
      <td>4.294982</td>
      <td>7.743011</td>
      <td>32.949821</td>
      <td>18.232975</td>
      <td>6.780287</td>
      <td>1.739068</td>
      <td>1.463441</td>
      <td>1.688530</td>
      <td>1.889247</td>
    </tr>
    <tr>
      <th>Badajoz</th>
      <td>13.981774</td>
      <td>2.197742</td>
      <td>8.091935</td>
      <td>27.193548</td>
      <td>17.250000</td>
      <td>0.546935</td>
      <td>0.099677</td>
      <td>0.061452</td>
      <td>0.080645</td>
      <td>0.305161</td>
    </tr>
    <tr>
      <th>Barcelona</th>
      <td>11.641505</td>
      <td>1.374409</td>
      <td>6.509462</td>
      <td>29.066667</td>
      <td>16.169892</td>
      <td>0.471183</td>
      <td>0.138925</td>
      <td>0.254409</td>
      <td>0.015269</td>
      <td>0.062581</td>
    </tr>
    <tr>
      <th>Bizkaia</th>
      <td>9.935484</td>
      <td>4.251613</td>
      <td>7.095699</td>
      <td>35.903226</td>
      <td>20.612903</td>
      <td>9.092473</td>
      <td>2.565054</td>
      <td>2.340860</td>
      <td>1.703226</td>
      <td>2.483333</td>
    </tr>
    <tr>
      <th>Burgos</th>
      <td>7.963343</td>
      <td>-1.152786</td>
      <td>3.408504</td>
      <td>35.117302</td>
      <td>21.337243</td>
      <td>1.789443</td>
      <td>0.378592</td>
      <td>0.291789</td>
      <td>0.421701</td>
      <td>0.697361</td>
    </tr>
    <tr>
      <th>Cantabria</th>
      <td>10.362366</td>
      <td>3.130824</td>
      <td>6.747312</td>
      <td>34.867384</td>
      <td>19.806452</td>
      <td>8.797133</td>
      <td>2.280287</td>
      <td>2.204659</td>
      <td>2.016487</td>
      <td>2.295699</td>
    </tr>
    <tr>
      <th>Castelló/Castellón</th>
      <td>12.151613</td>
      <td>2.311290</td>
      <td>7.234274</td>
      <td>46.786290</td>
      <td>27.669355</td>
      <td>0.304032</td>
      <td>0.086290</td>
      <td>0.055645</td>
      <td>0.069355</td>
      <td>0.092742</td>
    </tr>
    <tr>
      <th>Ceuta</th>
      <td>16.893548</td>
      <td>11.690323</td>
      <td>14.296774</td>
      <td>40.774194</td>
      <td>19.548387</td>
      <td>1.680645</td>
      <td>0.470968</td>
      <td>0.274194</td>
      <td>0.129032</td>
      <td>0.806452</td>
    </tr>
    <tr>
      <th>Ciudad Real</th>
      <td>12.553763</td>
      <td>0.368280</td>
      <td>6.466129</td>
      <td>25.650538</td>
      <td>12.962366</td>
      <td>0.667742</td>
      <td>0.300538</td>
      <td>0.141398</td>
      <td>0.018817</td>
      <td>0.206989</td>
    </tr>
    <tr>
      <th>Cuenca</th>
      <td>11.349597</td>
      <td>-1.510887</td>
      <td>4.914919</td>
      <td>33.431452</td>
      <td>18.096774</td>
      <td>0.462903</td>
      <td>0.145161</td>
      <td>0.029032</td>
      <td>0.025806</td>
      <td>0.262903</td>
    </tr>
    <tr>
      <th>Cáceres</th>
      <td>12.834140</td>
      <td>2.655108</td>
      <td>7.745968</td>
      <td>30.719086</td>
      <td>18.083333</td>
      <td>1.127554</td>
      <td>0.209946</td>
      <td>0.061828</td>
      <td>0.232796</td>
      <td>0.622984</td>
    </tr>
    <tr>
      <th>Cádiz</th>
      <td>15.852581</td>
      <td>6.086452</td>
      <td>10.973548</td>
      <td>31.432258</td>
      <td>18.980645</td>
      <td>1.730645</td>
      <td>0.517097</td>
      <td>0.372581</td>
      <td>0.119355</td>
      <td>0.721613</td>
    </tr>
    <tr>
      <th>Córdoba</th>
      <td>14.060573</td>
      <td>1.767025</td>
      <td>7.915771</td>
      <td>26.315412</td>
      <td>15.935484</td>
      <td>0.582079</td>
      <td>0.160573</td>
      <td>0.096057</td>
      <td>0.043728</td>
      <td>0.281720</td>
    </tr>
    <tr>
      <th>Gipuzkoa</th>
      <td>8.743318</td>
      <td>3.516129</td>
      <td>6.133641</td>
      <td>34.336406</td>
      <td>17.479263</td>
      <td>9.310599</td>
      <td>2.297696</td>
      <td>2.355300</td>
      <td>1.800922</td>
      <td>2.856682</td>
    </tr>
    <tr>
      <th>Girona</th>
      <td>11.814888</td>
      <td>-0.041687</td>
      <td>5.887593</td>
      <td>36.230769</td>
      <td>17.754342</td>
      <td>0.436476</td>
      <td>0.024566</td>
      <td>0.153350</td>
      <td>0.178164</td>
      <td>0.080397</td>
    </tr>
    <tr>
      <th>Granada</th>
      <td>15.103226</td>
      <td>4.912903</td>
      <td>10.007527</td>
      <td>29.354839</td>
      <td>15.784946</td>
      <td>0.590323</td>
      <td>0.261290</td>
      <td>0.152688</td>
      <td>0.065591</td>
      <td>0.110753</td>
    </tr>
    <tr>
      <th>Guadalajara</th>
      <td>10.190323</td>
      <td>-2.034677</td>
      <td>4.082258</td>
      <td>32.661290</td>
      <td>16.879032</td>
      <td>0.650806</td>
      <td>0.177419</td>
      <td>0.124194</td>
      <td>0.109677</td>
      <td>0.239516</td>
    </tr>
    <tr>
      <th>Huelva</th>
      <td>15.991935</td>
      <td>4.492258</td>
      <td>10.243548</td>
      <td>27.958065</td>
      <td>16.319355</td>
      <td>0.579355</td>
      <td>0.136129</td>
      <td>0.021613</td>
      <td>0.093226</td>
      <td>0.328387</td>
    </tr>
    <tr>
      <th>Huesca</th>
      <td>9.062007</td>
      <td>-1.201254</td>
      <td>3.931541</td>
      <td>36.541219</td>
      <td>20.951613</td>
      <td>1.475269</td>
      <td>0.220789</td>
      <td>0.414695</td>
      <td>0.407885</td>
      <td>0.431900</td>
    </tr>
    <tr>
      <th>Illes Balears</th>
      <td>14.373253</td>
      <td>4.845161</td>
      <td>9.612500</td>
      <td>40.637097</td>
      <td>21.922043</td>
      <td>1.288978</td>
      <td>0.277554</td>
      <td>0.243683</td>
      <td>0.293548</td>
      <td>0.474194</td>
    </tr>
    <tr>
      <th>Jaén</th>
      <td>12.953629</td>
      <td>2.349597</td>
      <td>7.653629</td>
      <td>23.596774</td>
      <td>12.536290</td>
      <td>0.975403</td>
      <td>0.283468</td>
      <td>0.381452</td>
      <td>0.041935</td>
      <td>0.268548</td>
    </tr>
    <tr>
      <th>La Rioja</th>
      <td>8.316774</td>
      <td>0.629677</td>
      <td>4.471613</td>
      <td>39.296774</td>
      <td>22.870968</td>
      <td>1.772903</td>
      <td>0.389677</td>
      <td>0.403871</td>
      <td>0.441290</td>
      <td>0.538065</td>
    </tr>
    <tr>
      <th>Las Palmas</th>
      <td>19.341642</td>
      <td>12.987390</td>
      <td>16.167595</td>
      <td>35.885630</td>
      <td>21.768328</td>
      <td>0.249853</td>
      <td>0.059677</td>
      <td>0.036070</td>
      <td>0.046481</td>
      <td>0.107625</td>
    </tr>
    <tr>
      <th>León</th>
      <td>10.100000</td>
      <td>-2.183065</td>
      <td>3.958871</td>
      <td>25.580645</td>
      <td>15.370968</td>
      <td>2.371774</td>
      <td>0.419355</td>
      <td>0.507258</td>
      <td>0.817742</td>
      <td>0.627419</td>
    </tr>
    <tr>
      <th>Lleida</th>
      <td>9.047465</td>
      <td>-2.499539</td>
      <td>3.272350</td>
      <td>34.834101</td>
      <td>20.373272</td>
      <td>0.968203</td>
      <td>0.031336</td>
      <td>0.262673</td>
      <td>0.419355</td>
      <td>0.254839</td>
    </tr>
    <tr>
      <th>Lugo</th>
      <td>10.091398</td>
      <td>3.115054</td>
      <td>6.608602</td>
      <td>29.978495</td>
      <td>15.139785</td>
      <td>4.931183</td>
      <td>1.081720</td>
      <td>1.270968</td>
      <td>1.387097</td>
      <td>1.191398</td>
    </tr>
    <tr>
      <th>Madrid</th>
      <td>11.493011</td>
      <td>-0.903763</td>
      <td>5.295968</td>
      <td>32.287634</td>
      <td>17.435484</td>
      <td>1.102151</td>
      <td>0.271237</td>
      <td>0.151344</td>
      <td>0.271505</td>
      <td>0.408065</td>
    </tr>
    <tr>
      <th>Melilla</th>
      <td>17.283871</td>
      <td>10.203226</td>
      <td>13.745161</td>
      <td>39.645161</td>
      <td>21.032258</td>
      <td>0.438710</td>
      <td>0.296774</td>
      <td>0.025806</td>
      <td>0.006452</td>
      <td>0.109677</td>
    </tr>
    <tr>
      <th>Murcia</th>
      <td>16.409926</td>
      <td>3.513151</td>
      <td>9.962035</td>
      <td>32.245658</td>
      <td>18.729529</td>
      <td>0.025062</td>
      <td>0.013896</td>
      <td>0.008189</td>
      <td>0.002978</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>Málaga</th>
      <td>16.453102</td>
      <td>7.690571</td>
      <td>12.074442</td>
      <td>32.119107</td>
      <td>17.677419</td>
      <td>0.514392</td>
      <td>0.194045</td>
      <td>0.123573</td>
      <td>0.027047</td>
      <td>0.169727</td>
    </tr>
    <tr>
      <th>Navarra</th>
      <td>9.254839</td>
      <td>1.770968</td>
      <td>5.518280</td>
      <td>42.827957</td>
      <td>24.831541</td>
      <td>4.953763</td>
      <td>1.150896</td>
      <td>1.300358</td>
      <td>1.051613</td>
      <td>1.450896</td>
    </tr>
    <tr>
      <th>Ourense</th>
      <td>11.629032</td>
      <td>0.367742</td>
      <td>5.987097</td>
      <td>16.645161</td>
      <td>5.645161</td>
      <td>2.880645</td>
      <td>0.448387</td>
      <td>0.722581</td>
      <td>0.938710</td>
      <td>0.770968</td>
    </tr>
    <tr>
      <th>Palencia</th>
      <td>8.712903</td>
      <td>-2.071889</td>
      <td>3.323963</td>
      <td>34.285714</td>
      <td>20.557604</td>
      <td>0.932719</td>
      <td>0.074654</td>
      <td>0.107834</td>
      <td>0.357604</td>
      <td>0.392627</td>
    </tr>
    <tr>
      <th>Pontevedra</th>
      <td>12.854032</td>
      <td>4.472581</td>
      <td>8.665323</td>
      <td>28.677419</td>
      <td>15.298387</td>
      <td>6.032258</td>
      <td>1.229032</td>
      <td>1.656452</td>
      <td>1.900000</td>
      <td>1.246774</td>
    </tr>
    <tr>
      <th>Salamanca</th>
      <td>9.401882</td>
      <td>-1.515860</td>
      <td>3.944086</td>
      <td>33.236559</td>
      <td>20.637097</td>
      <td>0.671505</td>
      <td>0.116129</td>
      <td>0.067742</td>
      <td>0.145699</td>
      <td>0.341935</td>
    </tr>
    <tr>
      <th>Santa Cruz de Tenerife</th>
      <td>17.447628</td>
      <td>10.214991</td>
      <td>13.833017</td>
      <td>35.182163</td>
      <td>21.303605</td>
      <td>0.784820</td>
      <td>0.215560</td>
      <td>0.194307</td>
      <td>0.156546</td>
      <td>0.218406</td>
    </tr>
    <tr>
      <th>Segovia</th>
      <td>8.146237</td>
      <td>-1.939785</td>
      <td>3.103226</td>
      <td>30.473118</td>
      <td>19.994624</td>
      <td>1.204301</td>
      <td>0.284946</td>
      <td>0.081720</td>
      <td>0.329032</td>
      <td>0.508602</td>
    </tr>
    <tr>
      <th>Sevilla</th>
      <td>15.679211</td>
      <td>3.438710</td>
      <td>9.559857</td>
      <td>26.114695</td>
      <td>15.695341</td>
      <td>0.584229</td>
      <td>0.136559</td>
      <td>0.037634</td>
      <td>0.055556</td>
      <td>0.354480</td>
    </tr>
    <tr>
      <th>Soria</th>
      <td>8.900806</td>
      <td>-2.257258</td>
      <td>3.324597</td>
      <td>40.822581</td>
      <td>25.991935</td>
      <td>1.229435</td>
      <td>0.299194</td>
      <td>0.171774</td>
      <td>0.231855</td>
      <td>0.526613</td>
    </tr>
    <tr>
      <th>Tarragona</th>
      <td>10.874194</td>
      <td>2.454839</td>
      <td>6.663871</td>
      <td>45.354839</td>
      <td>25.141935</td>
      <td>0.306452</td>
      <td>0.076774</td>
      <td>0.124516</td>
      <td>0.047097</td>
      <td>0.058065</td>
    </tr>
    <tr>
      <th>Teruel</th>
      <td>9.496774</td>
      <td>-0.568238</td>
      <td>4.469479</td>
      <td>43.774194</td>
      <td>26.210918</td>
      <td>0.512655</td>
      <td>0.165509</td>
      <td>0.075434</td>
      <td>0.064764</td>
      <td>0.206948</td>
    </tr>
    <tr>
      <th>Toledo</th>
      <td>11.882258</td>
      <td>0.271774</td>
      <td>6.076613</td>
      <td>30.137097</td>
      <td>18.766129</td>
      <td>0.346774</td>
      <td>0.066129</td>
      <td>0.025806</td>
      <td>0.054032</td>
      <td>0.200806</td>
    </tr>
    <tr>
      <th>Valladolid</th>
      <td>9.102151</td>
      <td>-2.666129</td>
      <td>3.220968</td>
      <td>28.629032</td>
      <td>17.467742</td>
      <td>0.779570</td>
      <td>0.059140</td>
      <td>0.022581</td>
      <td>0.300000</td>
      <td>0.397849</td>
    </tr>
    <tr>
      <th>València/Valencia</th>
      <td>15.633065</td>
      <td>2.863710</td>
      <td>9.250269</td>
      <td>32.115591</td>
      <td>17.169355</td>
      <td>0.058065</td>
      <td>0.037097</td>
      <td>0.005376</td>
      <td>0.004301</td>
      <td>0.011290</td>
    </tr>
    <tr>
      <th>Zamora</th>
      <td>9.522258</td>
      <td>-1.829677</td>
      <td>3.847742</td>
      <td>32.119355</td>
      <td>20.503226</td>
      <td>0.672903</td>
      <td>0.017419</td>
      <td>0.103226</td>
      <td>0.162581</td>
      <td>0.389677</td>
    </tr>
    <tr>
      <th>Zaragoza</th>
      <td>10.728387</td>
      <td>0.771935</td>
      <td>5.754516</td>
      <td>42.619355</td>
      <td>24.951613</td>
      <td>0.489677</td>
      <td>0.177419</td>
      <td>0.055484</td>
      <td>0.058710</td>
      <td>0.198065</td>
    </tr>
    <tr>
      <th>Ávila</th>
      <td>9.515207</td>
      <td>-0.869124</td>
      <td>4.321659</td>
      <td>32.576037</td>
      <td>19.990783</td>
      <td>0.626728</td>
      <td>0.152995</td>
      <td>0.016590</td>
      <td>0.176959</td>
      <td>0.280184</td>
    </tr>
  </tbody>
</table>
</div>



### Mayores temperaturas maximas por estación 

Vamos a mostrar los 3 valores mas altos de temperatura máxima que se han registrado en el mes.
Es un ejemplo de como ordenar por columna, mostrar solo las columnas que nos interesa y de esas columnas mostramos solo las primeras 3 filas


```python
df.sort_values(by='temp_max',ascending=False)[['fecha','estacion','provincia','temp_max']].head(3)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>fecha</th>
      <th>estacion</th>
      <th>provincia</th>
      <th>temp_max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>437</th>
      <td>2019-01-13</td>
      <td>La Aldea de San Nicolás</td>
      <td>Las Palmas</td>
      <td>27.8</td>
    </tr>
    <tr>
      <th>427</th>
      <td>2019-01-26</td>
      <td>Pájara</td>
      <td>Las Palmas</td>
      <td>25.7</td>
    </tr>
    <tr>
      <th>437</th>
      <td>2019-01-01</td>
      <td>La Aldea de San Nicolás</td>
      <td>Las Palmas</td>
      <td>25.6</td>
    </tr>
  </tbody>
</table>
</div>



### Menores temperaturas minimas por estación  

De forma similar obtenemos los 3 valores más bajos de temperatura minima


```python
df.sort_values(by='temp_min',ascending=True)[['fecha','estacion','provincia','temp_min']].head(3)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>fecha</th>
      <th>estacion</th>
      <th>provincia</th>
      <th>temp_min</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>522</th>
      <td>2019-01-21</td>
      <td>Villanueva de la Cañada</td>
      <td>Madrid</td>
      <td>-20.5</td>
    </tr>
    <tr>
      <th>639</th>
      <td>2019-01-06</td>
      <td>Cuéllar</td>
      <td>Segovia</td>
      <td>-12.1</td>
    </tr>
    <tr>
      <th>763</th>
      <td>2019-01-06</td>
      <td>Sardón de Duero</td>
      <td>Valladolid</td>
      <td>-11.8</td>
    </tr>
  </tbody>
</table>
</div>



### Maximas diferencias entre temperatura max y min por provincia

Ahora calculamos una nueva columna restando de la temperatura máxima la temperatura mínima. Vemos que simplemente restamos columnas. Pandas es intuitivo.


```python
df['diff_temp_min_max']=df['temp_max']-df['temp_min']
df.iloc[df.reset_index().groupby('provincia')['diff_temp_min_max'].idxmax()].sort_values(by='diff_temp_min_max',ascending=False)[['fecha','estacion','provincia','diff_temp_min_max','temp_min','temp_max']].head(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>fecha</th>
      <th>estacion</th>
      <th>provincia</th>
      <th>diff_temp_min_max</th>
      <th>temp_min</th>
      <th>temp_max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>522</th>
      <td>2019-01-21</td>
      <td>Villanueva de la Cañada</td>
      <td>Madrid</td>
      <td>30.9</td>
      <td>-20.5</td>
      <td>10.4</td>
    </tr>
    <tr>
      <th>763</th>
      <td>2019-01-06</td>
      <td>Sardón de Duero</td>
      <td>Valladolid</td>
      <td>26.6</td>
      <td>-11.8</td>
      <td>14.8</td>
    </tr>
    <tr>
      <th>639</th>
      <td>2019-01-06</td>
      <td>Cuéllar</td>
      <td>Segovia</td>
      <td>26.6</td>
      <td>-12.1</td>
      <td>14.5</td>
    </tr>
    <tr>
      <th>272</th>
      <td>2019-01-05</td>
      <td>Salvacañete</td>
      <td>Cuenca</td>
      <td>25.6</td>
      <td>-8.8</td>
      <td>16.8</td>
    </tr>
    <tr>
      <th>32</th>
      <td>2019-01-01</td>
      <td>Villena</td>
      <td>Alacant/Alicante</td>
      <td>25.5</td>
      <td>-6.2</td>
      <td>19.3</td>
    </tr>
    <tr>
      <th>665</th>
      <td>2019-01-15</td>
      <td>Burgo de Osma</td>
      <td>Soria</td>
      <td>25.3</td>
      <td>-7.6</td>
      <td>17.7</td>
    </tr>
    <tr>
      <th>299</th>
      <td>2019-01-05</td>
      <td>La Vall de Bianya</td>
      <td>Girona</td>
      <td>25.0</td>
      <td>-4.4</td>
      <td>20.6</td>
    </tr>
    <tr>
      <th>317</th>
      <td>2019-01-15</td>
      <td>Molina de Aragón</td>
      <td>Guadalajara</td>
      <td>24.9</td>
      <td>-8.5</td>
      <td>16.4</td>
    </tr>
    <tr>
      <th>223</th>
      <td>2019-01-06</td>
      <td>Valderredible, Cubillo de Ebro</td>
      <td>Cantabria</td>
      <td>24.3</td>
      <td>-11.3</td>
      <td>13.0</td>
    </tr>
    <tr>
      <th>472</th>
      <td>2019-01-02</td>
      <td>Martinet</td>
      <td>Lleida</td>
      <td>24.1</td>
      <td>-6.2</td>
      <td>17.9</td>
    </tr>
  </tbody>
</table>
</div>



### Mayores rachas de viento en un día por provincia
Vemos ahora un ejemplo de agrupar por una columna (provincia), obtener el máximo de los valores de otra columna (viento_racha) para cada grupo y ordenar posteriormente esas filas. Todo se realiza en una sola línea


```python
df.iloc[df.reset_index().groupby('provincia')['viento_racha'].idxmax()].sort_values(by='viento_racha',ascending=False)[['fecha','estacion','provincia','viento_racha']].head(5)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>fecha</th>
      <th>estacion</th>
      <th>provincia</th>
      <th>viento_racha</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2019-01-29</td>
      <td>Estaca de Bares</td>
      <td>A Coruña</td>
      <td>146.0</td>
    </tr>
    <tr>
      <th>139</th>
      <td>2019-01-29</td>
      <td>Machichaco</td>
      <td>Bizkaia</td>
      <td>144.0</td>
    </tr>
    <tr>
      <th>231</th>
      <td>2019-01-27</td>
      <td>La Pobla de Benifassà-Fredes</td>
      <td>Castelló/Castellón</td>
      <td>129.0</td>
    </tr>
    <tr>
      <th>370</th>
      <td>2019-01-27</td>
      <td>Sierra de Alfabia, Bunyola</td>
      <td>Illes Balears</td>
      <td>127.0</td>
    </tr>
    <tr>
      <th>302</th>
      <td>2019-01-25</td>
      <td>Espolla</td>
      <td>Girona</td>
      <td>111.0</td>
    </tr>
  </tbody>
</table>
</div>



### Mayores velocidades de viento medio en un día por provincia

Hacemos de forma similar el cálculo de las mayores velocidades de viento medio en un día por provincia.


```python
df.iloc[df.reset_index().groupby('provincia')['viento_vel_max'].idxmax()].sort_values(by='viento_vel_max',ascending=False)[['fecha','estacion','provincia','viento_vel_max']].head(5)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>fecha</th>
      <th>estacion</th>
      <th>provincia</th>
      <th>viento_vel_max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>139</th>
      <td>2019-01-23</td>
      <td>Machichaco</td>
      <td>Bizkaia</td>
      <td>113.0</td>
    </tr>
    <tr>
      <th>0</th>
      <td>2019-01-29</td>
      <td>Estaca de Bares</td>
      <td>A Coruña</td>
      <td>103.0</td>
    </tr>
    <tr>
      <th>726</th>
      <td>2019-01-28</td>
      <td>Andorra</td>
      <td>Teruel</td>
      <td>82.0</td>
    </tr>
    <tr>
      <th>688</th>
      <td>2019-01-17</td>
      <td>Izaña</td>
      <td>Santa Cruz de Tenerife</td>
      <td>80.0</td>
    </tr>
    <tr>
      <th>46</th>
      <td>2019-01-28</td>
      <td>Láujar de Andarax</td>
      <td>Almería</td>
      <td>78.0</td>
    </tr>
  </tbody>
</table>
</div>



### Maximas velocidades de viento por provincia de media
Vemos un ejemplo de calcular una media por grupo de la forma mas fácil e intuitiva posible (con "group" y "mean").


```python
df.groupby('provincia').mean()[['viento_vel_max']].sort_values(by='viento_vel_max',ascending=False).head(5)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>viento_vel_max</th>
    </tr>
    <tr>
      <th>provincia</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Castelló/Castellón</th>
      <td>27.669355</td>
    </tr>
    <tr>
      <th>Almería</th>
      <td>27.166667</td>
    </tr>
    <tr>
      <th>A Coruña</th>
      <td>27.073733</td>
    </tr>
    <tr>
      <th>Teruel</th>
      <td>26.210918</td>
    </tr>
    <tr>
      <th>Soria</th>
      <td>25.991935</td>
    </tr>
  </tbody>
</table>
</div>



### Maximas diferencias de temperatura maxima entre días
Vamos a ver un ejemplo de como podemos hacer una operación (restar) entre la fila actual y la fila anterior de forma sencilla: con un "shift" desplazamos una columna hacia abajo.


```python
df['temp_max_shifted']=df.groupby('estacion')['temp_max'].transform('shift')
df['dif_prev_temp_max']=df.temp_max-df.temp_max_shifted

df.iloc[df.reset_index().groupby('provincia')['dif_prev_temp_max'].idxmax()].sort_values(by='dif_prev_temp_max',ascending=False)[['fecha','estacion','provincia','dif_prev_temp_max']].head(5)

```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>fecha</th>
      <th>estacion</th>
      <th>provincia</th>
      <th>dif_prev_temp_max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>505</th>
      <td>2019-01-01</td>
      <td>Rascafría</td>
      <td>Madrid</td>
      <td>16.3</td>
    </tr>
    <tr>
      <th>605</th>
      <td>2019-01-01</td>
      <td>Velilla del Río Carrión, Camporredondo de Alba</td>
      <td>Palencia</td>
      <td>15.5</td>
    </tr>
    <tr>
      <th>470</th>
      <td>2019-01-01</td>
      <td>Lagunas de Somoza</td>
      <td>León</td>
      <td>14.5</td>
    </tr>
    <tr>
      <th>299</th>
      <td>2019-01-05</td>
      <td>La Vall de Bianya</td>
      <td>Girona</td>
      <td>13.6</td>
    </tr>
    <tr>
      <th>343</th>
      <td>2019-01-05</td>
      <td>Aragüés del Puerto</td>
      <td>Huesca</td>
      <td>13.6</td>
    </tr>
  </tbody>
</table>
</div>



### Racha de días seguidos lloviendo por provincia y estación
El proceso de obtener rachas, es decir, número de días en los que pasa un suceso (en este caso llover) consecutivamente es un proceso algo más elaborado pero que se puede realizar en unas pocas lineas de código aprovechando las siguientes funcionalidades del dataframe:
- Creación de una nueva columna cuyo valor se obtiene a partir de otra columna aplicando una función lambda (cálculo de la columna "llueve")
- Comparación de valores (operador "ne") entre columnas de una misma fila
- Desplazamiento (método "shift") de una columna para disponer de datos anteriores en la misma fila
- Generación de un número secuencial (identificador) de los elementos de cada grupo (método "cumsum")
- Cálculo del número de elementos de un grupo (método "cumcount")
- Asignación de un valor a una columna para las filas que cumplan una condición (llueve==0)


```python
df['llueve']=df.prec_dia.apply(lambda x: 1 if x>0 else 0)

df.sort_values(by=['estacion','fecha'],ascending=True,inplace=True,ignore_index=True)

df['start_of_streak'] = df.llueve.ne(df['llueve'].shift())

df['estacion_anterior']=df['estacion'].shift()
df.loc[(df['estacion'].ne(df['estacion_anterior'])), 'start_of_streak'] = True

df['streak_id'] = df['start_of_streak'].cumsum()
df['dias_seguidos_lloviendo'] = df.groupby('streak_id').cumcount() + 1
df.loc[(df['llueve']==0), 'dias_seguidos_lloviendo'] = 0

df.iloc[df.reset_index().groupby(['provincia','estacion'])['dias_seguidos_lloviendo'].idxmax()].sort_values(by='dias_seguidos_lloviendo',ascending=False)[['estacion','provincia','dias_seguidos_lloviendo']].head(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>estacion</th>
      <th>provincia</th>
      <th>dias_seguidos_lloviendo</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>5083</th>
      <td>Elgeta</td>
      <td>Gipuzkoa</td>
      <td>25</td>
    </tr>
    <tr>
      <th>14631</th>
      <td>Zumarraga</td>
      <td>Gipuzkoa</td>
      <td>20</td>
    </tr>
    <tr>
      <th>5455</th>
      <td>Forua</td>
      <td>Bizkaia</td>
      <td>18</td>
    </tr>
    <tr>
      <th>9795</th>
      <td>Orozko</td>
      <td>Bizkaia</td>
      <td>18</td>
    </tr>
    <tr>
      <th>7966</th>
      <td>Lugo</td>
      <td>Lugo</td>
      <td>16</td>
    </tr>
    <tr>
      <th>7563</th>
      <td>Llanes</td>
      <td>Asturias</td>
      <td>16</td>
    </tr>
    <tr>
      <th>30</th>
      <td>A Coruña</td>
      <td>A Coruña</td>
      <td>16</td>
    </tr>
    <tr>
      <th>11903</th>
      <td>Santiago de Compostela</td>
      <td>A Coruña</td>
      <td>16</td>
    </tr>
    <tr>
      <th>7904</th>
      <td>Los Tojos, Bárcena Mayor</td>
      <td>Cantabria</td>
      <td>15</td>
    </tr>
    <tr>
      <th>9702</th>
      <td>Ordizia</td>
      <td>Gipuzkoa</td>
      <td>15</td>
    </tr>
  </tbody>
</table>
</div>



### Donde llueve menos días seguidos por provincia y estación

Aprovechando la nueva columna "dias_seguidos_lloviendo" podemos sacar esta nueva métrica


```python
df.iloc[df.reset_index().groupby(['provincia','estacion'])['dias_seguidos_lloviendo'].idxmax()].sort_values(by='dias_seguidos_lloviendo',ascending=True)[['estacion','provincia','dias_seguidos_lloviendo']].head(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>estacion</th>
      <th>provincia</th>
      <th>dias_seguidos_lloviendo</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>7781</th>
      <td>Lorca, Zarcilla de Ramos</td>
      <td>Murcia</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2542</th>
      <td>Belmonte</td>
      <td>Cuenca</td>
      <td>0</td>
    </tr>
    <tr>
      <th>12617</th>
      <td>Tazacorte</td>
      <td>Santa Cruz de Tenerife</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1116</th>
      <td>Almería Aeropuerto</td>
      <td>Almería</td>
      <td>0</td>
    </tr>
    <tr>
      <th>434</th>
      <td>Albox</td>
      <td>Almería</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3100</th>
      <td>Buñol</td>
      <td>València/Valencia</td>
      <td>0</td>
    </tr>
    <tr>
      <th>8525</th>
      <td>Mogán, Puerto Rico</td>
      <td>Las Palmas</td>
      <td>0</td>
    </tr>
    <tr>
      <th>13595</th>
      <td>Vilafranca del Penedès</td>
      <td>Barcelona</td>
      <td>1</td>
    </tr>
    <tr>
      <th>13473</th>
      <td>València, Viveros</td>
      <td>València/Valencia</td>
      <td>1</td>
    </tr>
    <tr>
      <th>7580</th>
      <td>Lleida</td>
      <td>Lleida</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>



### Mostrar cuantas estaciones hay por provincia y un total general

Vamos a ver un ejemplo de que podemos tener copias "temporales" del dataframe para hacer operaciones sin destruir el original.
Queremos obtener el número de estaciones meteorológicas que hay por cada provincia. 
Para ello nos quedamos con solo las columnas relevantes: provincia y estacion.
Luego eliminamos las filas duplicadas para tener los pares únicos de provincia-estacion.
Ya nos queda simplemente agrupar por provincia y sacar el número de filas por cada grupo.
Todo eso hacemos en 1 sola línea de código.

Finalmente añadimos otra fila adicional al resultado con el total de estaciones (un SUM tipico de Excel)


```python
df_estaciones=df[['provincia','estacion']].drop_duplicates().groupby('provincia').count()
df_estaciones.loc['Total de estaciones'] = df_estaciones['estacion'].sum()
df_estaciones
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>estacion</th>
    </tr>
    <tr>
      <th>provincia</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>A Coruña</th>
      <td>7</td>
    </tr>
    <tr>
      <th>Alacant/Alicante</th>
      <td>7</td>
    </tr>
    <tr>
      <th>Albacete</th>
      <td>6</td>
    </tr>
    <tr>
      <th>Almería</th>
      <td>6</td>
    </tr>
    <tr>
      <th>Araba/Álava</th>
      <td>4</td>
    </tr>
    <tr>
      <th>Asturias</th>
      <td>9</td>
    </tr>
    <tr>
      <th>Badajoz</th>
      <td>20</td>
    </tr>
    <tr>
      <th>Barcelona</th>
      <td>15</td>
    </tr>
    <tr>
      <th>Bizkaia</th>
      <td>6</td>
    </tr>
    <tr>
      <th>Burgos</th>
      <td>11</td>
    </tr>
    <tr>
      <th>Cantabria</th>
      <td>9</td>
    </tr>
    <tr>
      <th>Castelló/Castellón</th>
      <td>8</td>
    </tr>
    <tr>
      <th>Ceuta</th>
      <td>1</td>
    </tr>
    <tr>
      <th>Ciudad Real</th>
      <td>6</td>
    </tr>
    <tr>
      <th>Cuenca</th>
      <td>8</td>
    </tr>
    <tr>
      <th>Cáceres</th>
      <td>24</td>
    </tr>
    <tr>
      <th>Cádiz</th>
      <td>10</td>
    </tr>
    <tr>
      <th>Córdoba</th>
      <td>9</td>
    </tr>
    <tr>
      <th>Gipuzkoa</th>
      <td>7</td>
    </tr>
    <tr>
      <th>Girona</th>
      <td>13</td>
    </tr>
    <tr>
      <th>Granada</th>
      <td>3</td>
    </tr>
    <tr>
      <th>Guadalajara</th>
      <td>4</td>
    </tr>
    <tr>
      <th>Huelva</th>
      <td>10</td>
    </tr>
    <tr>
      <th>Huesca</th>
      <td>18</td>
    </tr>
    <tr>
      <th>Illes Balears</th>
      <td>24</td>
    </tr>
    <tr>
      <th>Jaén</th>
      <td>8</td>
    </tr>
    <tr>
      <th>La Rioja</th>
      <td>5</td>
    </tr>
    <tr>
      <th>Las Palmas</th>
      <td>22</td>
    </tr>
    <tr>
      <th>León</th>
      <td>4</td>
    </tr>
    <tr>
      <th>Lleida</th>
      <td>7</td>
    </tr>
    <tr>
      <th>Lugo</th>
      <td>3</td>
    </tr>
    <tr>
      <th>Madrid</th>
      <td>12</td>
    </tr>
    <tr>
      <th>Melilla</th>
      <td>1</td>
    </tr>
    <tr>
      <th>Murcia</th>
      <td>13</td>
    </tr>
    <tr>
      <th>Málaga</th>
      <td>13</td>
    </tr>
    <tr>
      <th>Navarra</th>
      <td>9</td>
    </tr>
    <tr>
      <th>Ourense</th>
      <td>1</td>
    </tr>
    <tr>
      <th>Palencia</th>
      <td>7</td>
    </tr>
    <tr>
      <th>Pontevedra</th>
      <td>4</td>
    </tr>
    <tr>
      <th>Salamanca</th>
      <td>12</td>
    </tr>
    <tr>
      <th>Santa Cruz de Tenerife</th>
      <td>17</td>
    </tr>
    <tr>
      <th>Segovia</th>
      <td>6</td>
    </tr>
    <tr>
      <th>Sevilla</th>
      <td>9</td>
    </tr>
    <tr>
      <th>Soria</th>
      <td>8</td>
    </tr>
    <tr>
      <th>Tarragona</th>
      <td>5</td>
    </tr>
    <tr>
      <th>Teruel</th>
      <td>13</td>
    </tr>
    <tr>
      <th>Toledo</th>
      <td>4</td>
    </tr>
    <tr>
      <th>Valladolid</th>
      <td>6</td>
    </tr>
    <tr>
      <th>València/Valencia</th>
      <td>12</td>
    </tr>
    <tr>
      <th>Zamora</th>
      <td>10</td>
    </tr>
    <tr>
      <th>Zaragoza</th>
      <td>10</td>
    </tr>
    <tr>
      <th>Ávila</th>
      <td>7</td>
    </tr>
    <tr>
      <th>Total de estaciones</th>
      <td>473</td>
    </tr>
  </tbody>
</table>
</div>



# 4. Conclusiones

Hemos visto como es muy sencillo realizar las siguientes tareas: carga de datos, limpieza de datos y extracción de información usando Pandas.
Las operaciones que haríamos en una base de datos (agrupar, calcular máximos y mínimos) e incluso algunas tareas más complejas como obtener rachas se hacen de forma sencilla, rápida y lo que es importante de forma programática. Esto último es importante porque el código puede ser repositorizado, trackeado y ejecutado de forma automatizada.

Vemos también que se puede sacar información relevante de cualquier dataset. No es necesario llegar a usar algoritmos de Machine Learning para sacar provecho de la información disponible. La explotación de los datos mediante librerias como Pandas hace que podamos extraer información valiosa de forma sencilla y rápida, algo que es sumamente importante para cualquier empresa, entidad o incluso a nivel particular. Podemos verlo como una mezcla entre Excel y SQL pero con las ventajas que hemos ido mostrando en este artículo.

Espero que os haya gustado este primer contacto con la liberia Pandas y os haya convencido de probarla. 
Es una librería ampliamente usada para las tareas de Machine Learning y Data Science pero es realmente útil para cualquier persona que tenga que tratar con datos.

Tambien espero que haya servido como primer contacto con los notebook de Jupyter, que son una herramienta increible para tener en un mismo sitio el código y la documentación. Ademas de que permiten compartirlo de forma sencilla, asi como ser un punto de partida para la experimentación con el código contenido.

Este notebook estará disponible en la siguiente url: XXXX
